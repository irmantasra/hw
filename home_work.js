/**
 * 1. Write two binary functions, add and mul,
 * that take two numbers and return their
 * sum and product.
 */
function add (a, b){
    return (a + b);
}

function mul (a, b){
    return (a * b);
}

/**
 * 2. Write a function that takes an argument
 * and returns a function that returns that
 * argument. 
 */

let identityf = function (a){
    return () => { return a;} ;
}

var idf = identityf(3);
//console.log("idf() - " + idf());

/**
 * 3. Write a function that adds from two invocations. 
 */

let addf = function (a){
    return (b) => { return add(a, b)};
}
//console.log("addf(3)(4) - " + addf(3)(4));

/**
 * 4. Write a function that takes a binary function,
 * and makes it callable with two invocations. 
 */

let applyf = function (x){
    return (a) => {
        return (b) => {return x(a, b)}
    }
}
addf = applyf(add);
//console.log("addf(3)(4) - " + addf(3)(4));
//console.log("applyf(mul)(5)(6) - " + applyf(mul)(5)(6));

/**
 * 5. Write a function that takes a function and an argument,
 * and returns a function that can supply a second argument.
 */
let curry = function(x, a){
    return (b) => {
        return x(a, b);
    }
}
let add3 = curry(add, 3);
//console.log("add3(4) - " + add3(4));
//console.log("curry(mul , 5)(6) - " + curry(mul , 5)(6));

/**
 * 6. Without writing any new functions,
 * show three ways to create the inc
 * function.
 */
//first
let inc = function(a){
    return add (a, 1);
}
//console.log("first way inc(5) using add(5, 1) - " + inc(5));

//second
inc = function(a){
    return addf(a)(1);
}
//console.log("second way inc(10) using addf(10)(1) - " + inc(10));

//third
inc = function(a){
    return ++a;
}
//console.log("third way inc(13) using ++13 - " + inc(13));

/**
 * 7. Write a function twice that takes a binary function
 * and returns a unary function that passes its argument
 * to the binary function twice.
 */
let twice = function(x){
    return (a) => {
        return x(a, a);     //?
    }
}

let double = twice(add);
//console.log("double(11) - " + double(11));

let square = twice(mul);
//console.log("square(11) - " + square(11));

/**
 * 8. Write a function composeu that takes
 * two unary functions and returns a unary
 * function that calls them both
 */
let composeu = function(x, y){
    return (a) => {
        return y(x(a, a), x(a, a));     //?
    }
}
//console.log("composeu(add, mul)(3) - " + composeu(add, mul)(3));

/**
 * 9. Write a function composeb that takes two binary functions
 * and returns a function that calls them both
 */
let composeb = function(x, y){
    return (a, b, c) => {
        return y(c, x(a, b));
    }
}
//console.log("composeb(add, mul)(2, 3, 5) - " + composeb(add, mul)(2, 3, 5));

/**
 * 10. Write a factory function that returns
 * two functions that implement an up/down
 * counter. 
 */
let counterf = function(a){
    return{
        inc: function(){
           return ++a;
        },
        dec: function(){
           return --a;
        }
    }
}
let counter = counterf(10);
//console.log("counter.inc() - " + counter.inc());
//console.log("counter.dec() - " + counter.dec());

/**
 * 11. Make an array wrapper object with methods
 * get, store, and append, such that an attacker
 * cannot get access to the private array
 */
let vector = function(){
    var arr = [];
    return{
        get: function (i){
            return arr[i];
        },
        store: function (i, v){
            arr[i] = v;
        },
        append:function(v){
            arr.push(v);
        }
    }
}
let myVector = vector();
myVector.append(7); 
myVector.store(1, 8);
//console.log("myVector.get(0) - ", myVector.get(0));
//console.log("myVector.get(1) - ", myVector.get(1));

/**
 * 12. Write a function that adds from many invocations,
 until it sees an empty invocation.
 */
function addg(a){
    if (a === undefined) return a;
    return (b) => {
        if (b !== undefined){
            return addg(add(a, b));
        }
        return a;
    }
}
//console.log("addg(3)(4)(5)() - " + addg(3)(4)(5)());
//console.log("addg(1)(2)(4)(8)() - " + addg(1)(2)(4)(8)());

/**
 * 13. Write a function that will take a binary function
 * and apply it to many invocations.
 */
function applyg(x){
    return function g(a){
        if (a === undefined) return a;
        return (b) => {
            if (b !== undefined)
            return g(x(a, b));
            return a;
        }
    }
}
//console.log("applyg(add)(3)(4)(5)() - " + applyg(add)(3)(4)(5)());
//console.log("applyg(mul)(3)(4)(5)() - " + applyg(mul)(3)(4)(5)());
//console.log("applyg(add)(1)(2)(4)(8)() - " + applyg(add)(1)(2)(4)(8)());

/**
 * 14. Make a function that returns a function
 * that will return the next fibonacci number.
 */

/**
 * Create a generic iterator function each(array,
 * function(element, index)), which can be used
 * to iterate over arrays
 */

var testArray = [0, 5, 2, 7, 12];

function each(arr, x){
    let index = 0;
    for(element of arr){
        if (x(element, index++) === false){
            break;    
        }
    };
}

each(testArray, function(element, index){
    //console.log("[" + index + "]="+ element);
    
    if(index === 2){ 
        return false;
    }
});

/**
 * 16
 */
/**
 * a) Make a function that makes a publish/subscribe object.
 * It will reliably deliver all publications to all
 * subscribers in the right order.
 */
function pubsub(){
    return {
        arr: [],
        subscribe: function (x){
            this.arr.push(x);
        },
        publish: function (data){
            for (fun of this.arr){
                fun(data);
            }
        }
    }
}

var myPubsub = pubsub(); 
myPubsub.subscribe(function(data){
    console.log('Subscriber one says: ' + data);
});
myPubsub.subscribe(function(data){
    console.log('Subscriber two says: ' + data);
}); 
myPubsub.subscribe(function(data){
    console.log('Subscriber three says: ' + data);
}); 
 
//myPubsub.publish('hello!'); 

/**
 * b) Make a function that use pubsub to crete event manager.
 * That attach an event handler function for one or more events.
 * c) Make posible to remove an event handler by name.
 */
function eventManager(){
    return {
        eventDict: [],
        index: 0,
        on: function (name, func){
            let isFunction = false;
            this.eventDict.forEach(
                function(element){
                    if (element.eventName === name){
                        element.funcPubSub.subscribe(func);
                        isFunction = true;
                    }
                }, this
            )
            if(!isFunction){
                let pubs = pubsub();
                pubs.subscribe(func);
                this.eventDict.push( {eventName: name, funcPubSub: pubs} );
            }            
        },
        fire: function (exeName, data){
            for (event of this.eventDict){
                if (event.eventName == exeName){
                    event.funcPubSub.publish(data)
                }
            }
        },
        off: function (delName){
            for ( i = 0; i < this.eventDict.length; i++){
                if(this.eventDict[i].eventName === delName) {
                    this.eventDict.splice(i,1);
                    i--;
                }
            }
        }
    }
}

var manager = eventManager();

manager.on('event1', function(data){
    console.log('event1_handler1 says: ' + data);
});
manager.on('event2', function(data){
        console.log('event2_handler1 says: ' + data);
});
manager.on('event2', function(data){
    console.log('event2_handler2 says: ' + data);
});
manager.on('event3', function(data){
        console.log('event3_handler1 says: ' + data);
});
manager.on('event2', function(data){
    console.log('event2_handler3 says: ' + data);
});

// execute all handlers only for event2
manager.fire('event2', 'hello!');

manager.off('event2');
manager.fire('event2', 'hello!');
// no handlers are executed

